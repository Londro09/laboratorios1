Proceso numeroPi		
	Definir  i, num Como Entero;	
	Definir sumatoria Como Real;	
	Escribir "Ingrese cantidad de aproximacion";	
	Leer num;		
	sumatoria <- 0;
	Para i<-1 hasta num con paso 1 Hacer
		
		Si i%2 = 0 Entonces
			sumatoria <- sumatoria - 1/(i*2-1);
		SiNo
			sumatoria <- sumatoria + 1/(i*2-1);
		FinSi		
	FinPara	
	Escribir 4*sumatoria;
FinProceso
