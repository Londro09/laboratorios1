Proceso Ejemplo_Acumulador
	Definir SueldoBase, i Como Entero;
	
	SueldoBase<-1000;
	
	Para i<-1 Hasta 3 Con Paso 1 Hacer
		Escribir"Aumento ", i, " -> $ ", SueldoBase;
		
		SueldoBase<-SueldoBase+1000;
		
	FinPara
	
	Escribir  "Sueldo Final -> $", SueldoBase;
	

FinProceso
