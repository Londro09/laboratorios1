Proceso VectoresPrueba
	Definir Vector, Suma, Promedio Como Real;
	Definir i,  Long Como Entero;
    
	i<-0;
	Long<-0;
	Suma<-0;
	Promedio<-0;
	
	Dimension  Vector[100];
	Escribir "Ingresar Cantidad de Numeros (menor a 100)";
	Leer Long;
	
	Para i<-0 Hasta Long-1 Con Paso 1 Hacer
		Escribir "Ingrese el valor ",i+1;
		Leer Vector[i];
	FinPara
	
	Para i<-0 Hasta Long-1 Con Paso 1 Hacer
		Suma<- Suma+ Vector[i];
	FinPara
	
	Escribir "El promedio de los numeros ingresados es:";
	Promedio<-Suma/Long;
	Escribir Promedio;
	
FinProceso